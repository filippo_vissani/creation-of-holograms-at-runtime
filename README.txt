Progetto con un cubo ancorato alla mesh e un menu che permette di creare ologrammi (Rover) a runtime
i rover vengono generati partendo dalla posizione del cubo e successivamente vengono resi figli del cubo.
Al cubo e ai rover generati viene applicato il componente ObjectManipulator.
Muovendo il cubo si muoveranno anche i Rover generati.
Al riavvio dell'applicazione sarà presente solo il cubo ancorato alla mesh.