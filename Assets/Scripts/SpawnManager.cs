﻿using Microsoft.MixedReality.Toolkit.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public GameObject parent;
    public GameObject prefab;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SpawnNewPrefab()
    {
        GameObject childObject = Instantiate(prefab, new Vector3((float)(parent.transform.position.x + 0.5), parent.transform.position.y, parent.transform.position.z), parent.transform.rotation);
        childObject.transform.parent = parent.transform;
        childObject.AddComponent<ObjectManipulator>();
    }
}
